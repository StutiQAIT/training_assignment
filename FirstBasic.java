import java.util.Scanner;



public class FirstBasic {

	public static void main(String args[]){
		
		  int remainder,sum=0,temp;    
		  Scanner scan = new Scanner(System.in);
		  int number = scan.nextInt(); 		 //It is the number variable to be checked for palindrome  

		  temp=number;    
		  while(number>0){    
		   remainder=number%10;  //getting remainder  
		   sum=(sum*10)+remainder;    
		   number=number/10;    
		  }    
		  if(temp==sum)    
		   System.out.println("palindrome number ");    
		  else    
		   System.out.println("not palindrome");    

	}
}
